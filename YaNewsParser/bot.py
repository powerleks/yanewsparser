from bs4 import BeautifulSoup
from datetime import datetime
import django
import logging
from lxml.html import fromstring
import os
import pytz
import requests
import sys
import time
import telebot
from telebot import types
import urllib

sys.path.append('./YaNewsParser/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
django.setup()
from web.models import Query, SavedNews

TOKEN = os.environ['TOKEN']

# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)

bot = telebot.TeleBot(TOKEN)

#apihelper.proxy = {'https':'socks5://127.0.0.1:1723'}

def make_inline_keyboard(query):
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Ещё", callback_data=query))
    return markup

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.send_message(message.chat.id, "Введите поисковый запрос для новости, которую желаете увидеть. Бот выдаст "
                                      "первую новость по данной тематике в сервисе Яндекс.Новости")

def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()
    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.add(proxy)
    return proxies

def use_proxy(req):
    proxies = get_proxies()
    for proxy in proxies:
        news = get_news(req, proxy, True)
        if len(news) and '\n'.join(news[0]) != 'Ничего не найдено':
            return news
    return ('Ничего не найдено',)

def extract_text_from_html(soup):
    news = soup.findAll('div', {'class': 'document i-bem'})
    user_news = []
    if not len(news):
        user_news.append(('Ничего не найдено',))
    for chunk in news:
        if not chunk:
            user_news.append(('Ничего не найдено',))
        else:
            title = '<b>' + chunk.find('h2').find('div').find('a').text + '</b>'
            url = chunk.find('h2').find('div').find('a').get('href')
            text = chunk.find('div', {'class': 'document__snippet'}).text
            user_news.append((title, url, text))
    return user_news

def save_news(message_id, news):
    for i, text in enumerate(news):
        n = SavedNews(text=text, message_id=message_id, order=i)
        n.save()

def get_news(req, proxy=None, use_proxy=False):
    headers = {
        'Referer': 'https://news.yandex.ru',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
    }
    req = urllib.parse.quote(req, encoding="utf-8")
    url = 'https://news.yandex.ru/yandsearch?text={}&rpt=nnews2&grhow=clutop'.format(req)
    proxies = {"http": proxy, "https": proxy} if use_proxy else None 
    r = requests.get(url, headers=headers, proxies=proxies)
    soup = BeautifulSoup(r.text, 'lxml')
    news = extract_text_from_html(soup)
    return news

def get_next_text(message_id):
    news = SavedNews.objects.filter(message_id=message_id).order_by('order').first()
    if news:
        text = news.text
        news.delete()
    else:
        text = 'Больше ничего не найдено'
    return text

@bot.message_handler()
def main_handler(message):
    date = datetime.utcfromtimestamp(message.date)
    local_tz = pytz.timezone('Europe/Moscow')
    date = date.replace(tzinfo=pytz.utc).astimezone(local_tz)
    bot.send_message(message.chat.id, 'Пожалуйста, подождите! Загрузка новостей может занять какое-то время.', reply_markup=None)
    news = get_news(message.text)
    if '\n'.join(news[0]) == 'Ничего не найдено':
        news = use_proxy(message.text)
    news = list(map(lambda s: "\n".join(s), news))
    answer = news[0]
    markup = None if answer == 'Ничего не найдено' else make_inline_keyboard(message.text)
    q = Query(query=message.text, answer=answer, date=date)
    q.save()
    msg_sent = bot.send_message(message.chat.id, answer, reply_markup=markup, parse_mode='HTML')
    message_id = msg_sent.message_id
    save_news(message_id, news[1:])

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    query = call.data
    text = get_next_text(call.message.message_id)
    markup = None if text == 'Больше ничего не найдено' else make_inline_keyboard(query)
    date = datetime.utcnow()
    local_tz = pytz.timezone('Europe/Moscow')
    date = date.replace(tzinfo=pytz.utc).astimezone(local_tz)
    q = Query(query=query, answer=text, date=date)
    q.save()
    bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                          text=text, reply_markup=markup, parse_mode='HTML')

def main():
    while True:
        try:
            bot.polling(none_stop=True)

        except Exception as e:
            logger.error(e)
            time.sleep(15)

if __name__ == '__main__':
    main()
