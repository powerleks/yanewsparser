from django.db import models

class Query(models.Model):
    query = models.CharField(default="Nothing", max_length=100)
    answer = models.CharField(default="Nothing", max_length=1000)
    date = models.DateTimeField()

class SavedNews(models.Model):
    text = models.CharField(default="Nothing", max_length=1000)
    message_id = models.IntegerField(default=-1)
    order = models.IntegerField(default=-1)