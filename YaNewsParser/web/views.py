from django.db.models import Count
from django.db.models.functions import Trunc, TruncMonth
from django.shortcuts import render
from django.http import HttpRequest
from datetime import datetime, date, timedelta
from web.models import Query

def get_day_chart():
    last_7_days = datetime.today() - timedelta(7)
    reqs = Query.objects.filter(date__gte=last_7_days).values("date")
    q_day = reqs.annotate(day=Trunc('date', 'day')).values('day')
    count = q_day.annotate(c=Count('id'))
    chart = {}
    chart['title'] = 'Number of requests for the last week'
    d = {}
    chart['categories'] = []
    for i in range(7):
        day = (datetime.today() - timedelta(6 - i)).strftime("%d.%m.%Y")
        d[day] = 0
        chart['categories'].append(day)
    chart['data'] = []
    for req in count:
        d[req['day'].strftime("%d.%m.%Y")] = req['c']
    for i in range(7):
        day = (datetime.today() - timedelta(6 - i)).strftime("%d.%m.%Y")
        chart['data'].append(d[day])
    return chart

def get_month_chart():
    last_year = datetime.today() - timedelta(365)
    reqs = Query.objects.filter(date__gte=last_year).values("date")
    q_month = Query.objects.annotate(month=TruncMonth('date')).values('month')
    count = q_month.annotate(c=Count('id')).values('month', 'c')
    chart = {}
    chart['title'] = 'Number of requests for the last year per month'
    chart['categories'] = []
    chart['data'] = []
    d = {}
    for i in range(1, 13):
        d[date(2017, i, 1).strftime("%b")] = 0
    for req in count:
        month = req['month'].strftime("%b")
        d[month] = req['c']
    cur_m = int(datetime.now().strftime("%m"))
    for i in range((cur_m - 11) % 12, 13):
        month = date(2017, i, 1).strftime("%b")
        chart['categories'].append(month)
        chart['data'].append(d[month])
    if cur_m == 12:
        return chart
    for i in range(1, cur_m + 1):
        month = date(2017, i, 1).strftime("%b")
        chart['categories'].append(month)
        chart['data'].append(d[month])
    return chart

def top_queries():
    queries = Query.objects.values('query').annotate(dcount=Count('query')).order_by('-dcount')[:5]
    return queries

def top_today_queries():
    queries = Query.objects.filter(
        date__gte=date.today()).values('query').annotate(dcount=Count('query')).order_by('-dcount')[:5]
    return queries

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'web/index.html',
        {
            'title': 'Home Page',
            'top_queries': top_queries(),
            'top_today_queries': top_today_queries(),
            'year': datetime.now().year,
        }
    )

def charts(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'web/charts.html',
        {
            'title': 'Charts',
            'chart_day': get_day_chart(),
            'chart_month': get_month_chart(),
            'year': datetime.now().year,
        }
    )

def logs(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'web/logs.html',
        {
            'title': 'Logs',
            'queries': Query.objects.all(),
            'year': datetime.now().year,
        }
    )