# YaNewsParser

http://80.211.48.64/yaparser/

This repository contains source code of a telegram bot @powerlekstodo_bot (in the file YaNewsParser/bot.py) and a django project with bot's statistic. The bot can parse news on a given query from https://news.yandex.ru. The site contains all logs of bot's queries, most popular queries and charts with bot's usage.
